#!/usr/bin/env bash

set -e

docker build \
    -t podlesny/dune-2.5:latest 01-dune-2.5
