#!/usr/bin/env bash

set -e

tag=2017-KornhuberPodlesnyYserentant
docker build \
    -t podlesny/dune-osccoeff:${tag} 02-dune-osccoeff

tmpdir=`mktemp -d`
docker run --rm -v $tmpdir:/bridge podlesny/dune-osccoeff:${tag} \
    sh -c 'tar -C /dune-build/dune-osccoeff/src -cf /bridge/dune-osccoeff.tar \
               osccoeff \
               osccoeff3d && \
           tar -C /dune-sources/dune-osccoeff/src -rf /bridge/dune-osccoeff.tar \
               data/ \
               results/ \
               osccoeff.parset \
               osccoeff3d.parset && \
           tar -C /usr/lib/x86_64-linux-gnu -h -rf /bridge/dune-osccoeff.tar \
               libsuperlu.so.5 \
               libstdc++.so.6 && \
           tar -C /lib/x86_64-linux-gnu -h -rf /bridge/dune-osccoeff.tar \
               ld-linux-x86-64.so.2 \
               libc.so.6 \
               libdl.so.2 \
               libgcc_s.so.1 \
               libm.so.6 \
               libpthread.so.0 \
               libz.so.1'
mv $tmpdir/dune-osccoeff.tar .
rmdir $tmpdir
rm -rf dune-osccoeff
mkdir dune-osccoeff
tar -xvf dune-osccoeff.tar -C dune-osccoeff
